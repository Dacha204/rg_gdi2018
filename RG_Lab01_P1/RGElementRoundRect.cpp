#include "stdafx.h"
#include "RGElementRoundRect.h"


RGElementRoundRect::RGElementRoundRect(CDC * pDC)
	:RGDrawElement(pDC)
{
}

RGElementRoundRect::RGElementRoundRect(CDC * pDC, CPoint startCorner, CPoint endCorner)
	: RGDrawElement(pDC, startCorner, endCorner)
{
}

RGElementRoundRect::RGElementRoundRect(CDC * pDC, CPoint startCorner, int width, int height)
	: RGDrawElement(pDC, startCorner, width, height)
{
}

RGElementRoundRect::~RGElementRoundRect()
{
}

void RGElementRoundRect::DrawElement()
{
	pDC->RoundRect(
		getStartCorner()->x, getStartCorner()->y,
		getEndCorner()->x, getEndCorner()->y,
		roundness.x, roundness.y
	);
}

void RGElementRoundRect::SetRoundness(int pixelRoundness)
{
	roundness.x = roundness.y = pixelRoundness;
}

void RGElementRoundRect::SetRoundness(int RoundnessX, int RoundnessY)
{
	roundness.x = RoundnessX;
	roundness.y = RoundnessY;
}

CPoint RGElementRoundRect::GetRoundness()
{
	return roundness;
}
