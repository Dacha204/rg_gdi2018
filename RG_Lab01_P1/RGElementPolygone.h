#pragma once
#include "RGDrawElement.h"
class RGElementPolygone :
	public RGDrawElement
{
public:
	RGElementPolygone(CDC* pDC);
	//RGElementPolyLine(CDC* pDC, CPoint startCorner, CPoint endCorner);
	//RGElementPolyLine(CDC* pDC, CPoint startCorner, int width, int height);
	~RGElementPolygone();

	void DrawElement();
	void AddNextPoint(CPoint point);

};

