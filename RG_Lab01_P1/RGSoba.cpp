#include "stdafx.h"
#include "RGSoba.h"
#include "RGDrawElement.h"
#include "RGElementRect.h"
#include "RGElementEllipse.h"
#include "RGElementPolygone.h"
#include "RGElementRoundRect.h"
#include "DImage.h"

#define MYPEN_STROKE_DEFAULT 3
#define MYPEN_COLOR_DEFAULT RGB(66,33,0)
#define MYBRUSH_COLOR_WOOD RGB(132,99,66)
#define MYBRUSH_COLOR_WALL_CENTRAL RGB(255, 192, 128)
#define MYBRUSH_COLOR_WALL_SIDES RGB(195, 147, 98)
#define MYBRUSH_COLOR_HANDLE RGB(234, 136, 0)
#define MYBRUSH_COLOR_FLOOR RGB(192, 192, 192)
#define MYBRUSH_COLOR_WINDOWSGLASS RGB(29, 143, 170)
#define MYCOLOR_BLACK RGB(0,0,0)
#define MYCOLOR_WHITE RGB(255,255,255)

#define MYFILE_KEY "Key.emf"
#define MYFILE_SMALL_CURSOR "./MalaKazaljka.emf"
#define MYFILE_BIG_CURSOR "./VelikaKazaljka.emf"
#define MYFILE_PUZZLE_PART1 "part1.bmp"
#define MYFILE_PUZZLE_PART2 "part2.bmp"
#define MYFILE_PUZZLE_PART3 "part3.bmp"
#define MYFILE_PUZZLE_PART4 "part4.bmp"
#define MYFILE_PAINTING_FRAME "Ram.bmp"


#define MATH_PI 3.14159265359
float Angle(float angle)
{
	return MATH_PI / 180 * angle;
}
void PrimeniTransformaciju(CDC* pDC, float cx, float cy, float rx, float ry, float alfa, XFORM* oldXFORM)
{
	pDC->GetWorldTransform(oldXFORM);
	XFORM newXFROM;

	//translacija
	newXFROM.eM11 = 1;	//rotacija: cos;		skaliranje: horiz;	iskosenje: 1;					reflekcija: horiz.ref.komp
	newXFROM.eM12 = 0;	//rotacija: sin;		skaliranje: 0;		iskosenje: horiz.kons.prop;		refleksija: 0
	newXFROM.eM21 = 0;	//rotacija: -sin;		skaliranje: 0;		iskosenje: vertc.kons.prop;		refleksija: 0
	newXFROM.eM22 = 1;	//rotacija: cos;		skaliranje: vertc;	iskosenje: 1;					refleksija: vertc.ref.komp
	newXFROM.eDx = rx;	//horiz. translacija
	newXFROM.eDy = -ry;	//verticalna translacija

	pDC->SetWorldTransform(&newXFROM);
	
	//rotacija
	newXFROM.eM11 = cos(alfa);
	newXFROM.eM12 = sin(alfa);
	newXFROM.eM21 = -sin(alfa);
	newXFROM.eM22 = cos(alfa);
	newXFROM.eDx = 0;
	newXFROM.eDy = 0;

	pDC->ModifyWorldTransform(&newXFROM, MWT_RIGHTMULTIPLY);

	//translacija
	newXFROM.eM11 = 1;
	newXFROM.eM12 = 0;
	newXFROM.eM21 = 0;
	newXFROM.eM22 = 1;
	newXFROM.eDx = cx;
	newXFROM.eDy = -cy;

	pDC->ModifyWorldTransform(&newXFROM, MWT_LEFTMULTIPLY);
}

void SetBitmapTransform(CDC* pDC, float cx, float cy, float bw, float bh, float scale, float alpha, bool mirror, XFORM* oldXForm)
{
	pDC->GetWorldTransform(oldXForm);
	XFORM Xform;
	int m = mirror ? -1: 1;


	//skaliranje i mirror
	Xform.eM11 = m*scale;
	Xform.eM12 = 0.0;
	Xform.eM21 = 0.0;
	Xform.eM22 = scale;
	Xform.eDx = 0;
	Xform.eDy = 0;
	pDC->SetWorldTransform(&Xform);

	//rotacija
	Xform.eM11 = cos(alpha);
	Xform.eM12 = sin(alpha);
	Xform.eM21 = -sin(alpha);
	Xform.eM22 = cos(alpha);
	Xform.eDx = cx;
	Xform.eDy = cy;
	pDC->ModifyWorldTransform(&Xform, MWT_RIGHTMULTIPLY);

}
void SetBitmapRGBFilter(CBitmap* bmp, bool r, bool g, bool b)
{
	__int32 cmb = 0;
	if (r)
		cmb |= 0x00FF0000;
	if (g)
		cmb |= 0x0000FF00;
	if (b)
		cmb |= 0x000000FF;
	BITMAP bm;
	bmp->GetBitmap(&bm);
	int size = bm.bmWidth*bm.bmHeight*bm.bmBitsPixel;

	__int32* buff = new __int32[size];
	bmp->GetBitmapBits(size, buff);
	for (int i = 0, j = 0; i < size; i++, j++)
	{
		buff[i] &= cmb;
	}
	bmp->SetBitmapBits(size, buff);

}
void DrawBitmap(CDC* pDC, int x, int y, float scale, CString name)
{

	DImage* img = new DImage();
	img->Load(name);
	int w = img->Width();
	int h = img->Height();
	CRect src(0, 0, w, h);
	CRect dst(x, y, x + w*scale, y + h*scale);
	img->Draw(pDC, src, dst);
	delete img;
}

void DrawBitmapDIMAGE(CDC* pDC, int x, int y, float scale, DImage* image) //hack no2
{
	int w = image->Width();
	int h = image->Height();
	CRect src(0, 0, w, h);
	CRect dst(x, y, x + w*scale, y + h*scale);
	image->Draw(pDC, src, dst);
}

void DrawTransparentBitmapDIMAGE(CDC* pDC, int x, int y, float scale, bool r, bool g, bool b, DImage* p)
{
	
	int width = 350; //part width
	CRect srcRc(0, 0, width, width);
	CRect dstRc(0, 0, width, width);

	CBitmap bmpImage;
	bmpImage.CreateBitmap(p->Width(), p->Height(), 1, 32, NULL);

	CBitmap bmpMask;
	bmpMask.CreateBitmap(p->Width(), p->Height(), 1, 1, NULL);
	DImage* mask = new DImage(bmpMask);

	CDC* srcDC = new CDC();
	srcDC->CreateCompatibleDC(NULL);
	CDC* dstDC = new CDC();
	dstDC->CreateCompatibleDC(NULL);

	CBitmap* pOldSrcBmp = srcDC->SelectObject(&bmpImage);
	CBitmap* pOldDstBmp = dstDC->SelectObject(&bmpMask);

	p->Draw(srcDC, srcRc, dstRc);
	mask->Draw(dstDC, srcRc, dstRc);

	COLORREF clrTopLeft = srcDC->GetPixel(0, 0);
	COLORREF clrSaveBk = srcDC->SetBkColor(clrTopLeft);
	dstDC->BitBlt(0, 0, p->Width(), p->Height(), srcDC, x, y, SRCCOPY);

	srcDC->SetBkColor(RGB(0, 0, 0));
	int rc, gc, bc;
	rc = r ? 255 : 0;
	gc = g ? 255 : 0;
	bc = b ? 255 : 0;
	srcDC->SetTextColor(RGB(rc, gc, bc));
	srcDC->BitBlt(0, 0, p->Width(), p->Height(), dstDC, 0, 0, SRCAND);

	srcDC->SelectObject(pOldSrcBmp);
	srcDC->DeleteDC();
	delete srcDC;


	dstDC->SelectObject(pOldDstBmp);
	dstDC->DeleteDC();
	delete dstDC;

	CDC* memDC = new CDC();
	memDC->CreateCompatibleDC(NULL);
	CBitmap* bmpOld = memDC->SelectObject(&bmpMask);
	pDC->StretchBlt(0, 0, p->Width()*scale, p->Height()*scale, memDC, 0, 0, p->Width(), p->Height(), SRCAND);
	memDC->SelectObject(&bmpImage);
	pDC->StretchBlt(0, 0, p->Width()*scale, p->Height()*scale, memDC, 0, 0, p->Width(), p->Height(), SRCPAINT);
	memDC->SelectObject(bmpOld);
	memDC->DeleteDC();
	delete memDC;
	delete mask;
}

void DrawTransparentBitmap(CDC* pDC, int x, int y, float scale, bool r, bool g, bool b, CString name)
{
	DImage* p;
	p = new DImage();
	p->Load(name);
	
	int width = 350; //part width
	CRect srcRc(0, 0, width, width);
	CRect dstRc(0, 0, width, width);

	CBitmap bmpImage;
	bmpImage.CreateBitmap(p->Width(), p->Height(), 1, 32, NULL);
	
	CBitmap bmpMask;
	bmpMask.CreateBitmap(p->Width(), p->Height(), 1, 1, NULL);
	DImage* mask = new DImage(bmpMask);

	CDC* srcDC = new CDC();
	srcDC->CreateCompatibleDC(NULL);
	CDC* dstDC = new CDC();
	dstDC->CreateCompatibleDC(NULL);

	CBitmap* pOldSrcBmp = srcDC->SelectObject(&bmpImage);
	CBitmap* pOldDstBmp = dstDC->SelectObject(&bmpMask);

	p->Draw(srcDC, srcRc, dstRc);
	mask->Draw(dstDC, srcRc, dstRc);

	COLORREF clrTopLeft = srcDC->GetPixel(0, 0);
	COLORREF clrSaveBk = srcDC->SetBkColor(clrTopLeft);
	dstDC->BitBlt(0, 0, p->Width(), p->Height(), srcDC, x, y, SRCCOPY);

	srcDC->SetBkColor(RGB(0, 0, 0));
	int rc, gc, bc;
	rc = r ? 255 : 0;
	gc = g ? 255 : 0;
	bc = b ? 255 : 0;
	srcDC->SetTextColor(RGB(rc, gc, bc));
	srcDC->BitBlt(0, 0, p->Width(), p->Height(), dstDC, 0, 0, SRCAND);

	srcDC->SelectObject(pOldSrcBmp);
	srcDC->DeleteDC();
	delete srcDC;


	dstDC->SelectObject(pOldDstBmp);
	dstDC->DeleteDC();
	delete dstDC;

	CDC* memDC = new CDC();
	memDC->CreateCompatibleDC(NULL);
	CBitmap* bmpOld = memDC->SelectObject(&bmpMask);
	pDC->StretchBlt(0, 0, p->Width()*scale, p->Height()*scale, memDC, 0, 0, p->Width(), p->Height(), SRCAND);
	memDC->SelectObject(&bmpImage);
	pDC->StretchBlt(0, 0, p->Width()*scale, p->Height()*scale, memDC, 0, 0, p->Width(), p->Height(), SRCPAINT);
	memDC->SelectObject(bmpOld);
	memDC->DeleteDC();
	delete memDC;
	delete p;
	delete mask;
}
void DrawPuzzlePart(CDC* pDC, int x, int y, float scale, float angle, bool mirror, bool r, bool g, bool b, CString name)
{
	XFORM Xform;
	SetBitmapTransform(pDC, x, y, 350, 350, 1, angle, mirror, &Xform);
	DrawTransparentBitmap(pDC, 0, 0, scale, r, g, b, name);
	pDC->SetWorldTransform(&Xform);
}
void DrawPuzzlePartDIMAGE(CDC* pDC, int x, int y, float scale, float angle, bool mirror, bool r, bool g, bool b, DImage* name)
{
	XFORM Xform;
	SetBitmapTransform(pDC, x, y, 350, 350, 1, angle, mirror, &Xform);
	DrawTransparentBitmapDIMAGE(pDC, 0, 0, scale, r, g, b, name);
	pDC->SetWorldTransform(&Xform);
}


RGRoom::RGRoom()
{
	this->m_isDoorOpen = false;
	this->m_isWardrobeOpen = false;
	this->m_hasKey = false;

	this->WardrboeHitBox = CRect(118, 237, 241, 749);
	this->DoorHitBox = CRect(1810, 610, 1903, 665);
	this->KeyHitBox = CRect(118, 635, 241, 749);

	this->canvasSize = CPoint(1920, 1080);
	
	this->small_cursor_angle = 0;
	this->big_cursor_angle = 0;
	this->swing_angle = 0;
	this->swing_direction = 1;

	this->paintingColorRed = true;
	this->paintingColorGreen = true;
	this->paintingColorBlue = true;
}
RGRoom::~RGRoom()
{
}
void RGRoom::DrawRoom(CDC* pDC)
{
	this->pDC = pDC;

	DrawWalls();
	//DrawRoof();
	DrawFloor();
	DrawWardrobe();
	DrawDoor();
	DrawWindow();
	DrawGUI();
	//DrawEye();
	//DrawFlower(CPoint(962, 432), 50, 100);
	DrawClock();
	//DrawPainting();
	//_DrawHitBoxes();
	DrawKey();
	
}
bool RGRoom::SetOpenDoor()
{
	if (m_hasKey)
	{
		m_isDoorOpen = true;
		DoorHitBox = CRect(0, 0, 0, 0);
	}
	return m_isDoorOpen;
}
bool RGRoom::SetWardrobeOpen()
{
	m_isWardrobeOpen = true;
	this->WardrboeHitBox = CRect(0, 0, 0, 0);
	return m_isWardrobeOpen;
}
bool RGRoom::SetHasKey()
{
	if (m_isWardrobeOpen)
	{
		this->KeyHitBox = CRect(0, 0, 0, 0);
		m_hasKey = true;
	}
	return m_hasKey;
}
CRect RGRoom::GetWardrobeHitBox()
{
	return this->WardrboeHitBox;
}
CRect RGRoom::GetDoorHitBox()
{
	return this->DoorHitBox;
}
CRect RGRoom::GetKeyHitBox()
{
	return this->KeyHitBox;
}
void RGRoom::DrawWalls()
{
	int saveDCIndex = pDC->SaveDC();

	CPen pen_default(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brush_centralWall(MYBRUSH_COLOR_WALL_CENTRAL);
	CBrush brush_sideWalls(MYBRUSH_COLOR_WALL_SIDES);
	int sideWall_width = 192;
	int sideWall_perspective = 106;

	//CENTER WALL
	RGElementRect cntWall(pDC);
	cntWall.SetOffsetTop(106);
	cntWall.SetOffsetLeft(192);
	cntWall.SetHeight(869);
	cntWall.SetWidth(1535 + 1);
	cntWall.Draw(&pen_default, &brush_centralWall);

	//LEFT WALL
	RGElementPolygone leftWall(pDC);
	leftWall.AddNextPoint(CPoint(0, 0));
	leftWall.AddNextPoint(CPoint(sideWall_width, sideWall_perspective));
	leftWall.AddNextPoint(CPoint(sideWall_width, getCanvasSize().y - sideWall_perspective + 1));
	leftWall.AddNextPoint(CPoint(0, getCanvasSize().y));
	leftWall.Draw(&pen_default, &brush_sideWalls);
	
	//RIGHT WALL
	RGElementPolygone rightWall(pDC);
	rightWall.AddNextPoint(CPoint(getCanvasSize().x, 0));
	rightWall.AddNextPoint(CPoint(getCanvasSize().x, getCanvasSize().y));
	rightWall.AddNextPoint(CPoint(getCanvasSize().x - sideWall_width -1 , getCanvasSize().y - sideWall_perspective + 1));
	rightWall.AddNextPoint(CPoint(getCanvasSize().x - sideWall_width - 1, sideWall_perspective));
	rightWall.Draw(&pen_default, &brush_sideWalls);

	pDC->RestoreDC(saveDCIndex);
}
void RGRoom::DrawWardrobe()
{
	int SaveDCIndex = pDC->SaveDC();

	CPen penWardrobe(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brushWardrobe(MYBRUSH_COLOR_WOOD);
	CBrush brushHandle(MYBRUSH_COLOR_HANDLE);
	CBrush brushBlack(MYCOLOR_BLACK);

	int wardrobe_width = 290;
	int wardrobe_height = 815;

	int w_door_width = 123 + 1;
	int w_door_height = 512;
	
	int w_drawer_width = 246;
	int w_drawer_height = 110;
	int w_offset = 22;
	
	RGElementRect mainframe(pDC);
	RGElementRect leftDoor(pDC);
	RGElementRect rightDoor(pDC);
	RGElementRect upDrawer(pDC);
	RGElementRect downDrawer(pDC);
	CPoint wardrobe_topCorner(96, 213);

	mainframe.SetStartCorner(wardrobe_topCorner);
	mainframe.SetHeight(wardrobe_height);
	mainframe.SetWidth(wardrobe_width);
	mainframe.Draw(&penWardrobe, &brushWardrobe);

	leftDoor.SetStartCorner(wardrobe_topCorner + CPoint(w_offset, w_offset + 3));
	leftDoor.SetHeight(w_door_height);
	leftDoor.SetWidth(w_door_width);
	

	rightDoor.SetStartCorner(CPoint(leftDoor.GetStartCorner().x + w_door_width - 1, leftDoor.GetStartCorner().y));
	rightDoor.SetHeight(w_door_height);
	rightDoor.SetWidth(w_door_width);
	rightDoor.Draw(&penWardrobe, &brushWardrobe);

	upDrawer.SetStartCorner(CPoint(mainframe.GetStartCorner().x+ w_offset, rightDoor.GetEndCorner().y + w_offset));
	upDrawer.SetWidth(w_drawer_width);
	upDrawer.SetHeight(w_drawer_height);
	upDrawer.Draw(&penWardrobe, &brushWardrobe);

	downDrawer.SetStartCorner(CPoint(upDrawer.GetStartCorner().x, upDrawer.GetEndCorner().y + w_offset));
	downDrawer.SetWidth(w_drawer_width);
	downDrawer.SetHeight(w_drawer_height);
	downDrawer.Draw(&penWardrobe, &brushWardrobe);

	if (m_isWardrobeOpen)
	{
		leftDoor.Draw(&penWardrobe, &brushBlack);
		RGElementPolygone openDoor(pDC);
		openDoor.AddNextPoint(CPoint(leftDoor.GetStartCorner().x - 75, leftDoor.GetStartCorner().y - 25));
		openDoor.AddNextPoint(leftDoor.GetStartCorner());
		openDoor.AddNextPoint(CPoint(leftDoor.GetStartCorner().x, leftDoor.GetEndCorner().y));
		openDoor.AddNextPoint(CPoint(leftDoor.GetStartCorner().x - 75, leftDoor.GetEndCorner().y + 25));
		openDoor.Draw(&penWardrobe, &brushWardrobe);

		
	}
	else 
	{
		leftDoor.Draw(&penWardrobe, &brushWardrobe);
		RGElementEllipse handleLeft(pDC);
		handleLeft.SetStartCorner(CPoint(197, 491));
		handleLeft.SetHeight(25);
		handleLeft.SetWidth(25);
		handleLeft.Draw(&penWardrobe, &brushHandle);
	}

	int w_side_width = 98;
	int w_side_height = 705;
	int w_side_perspective = 53;

	RGElementPolygone sideWardrobe(pDC);
	sideWardrobe.AddNextPoint(CPoint(mainframe.GetEndCorner().x-1, mainframe.GetStartCorner().y));
	sideWardrobe.AddNextPoint(CPoint(mainframe.GetEndCorner().x + w_side_width, mainframe.GetStartCorner().y + w_side_perspective));
	sideWardrobe.AddNextPoint(CPoint(mainframe.GetEndCorner().x + w_side_width, mainframe.GetEndCorner().y - w_side_perspective));
	sideWardrobe.AddNextPoint(mainframe.GetEndCorner() - CPoint(1, 1));
	sideWardrobe.Draw(&penWardrobe, &brushWardrobe);

	//TODO: make it relative
	RGElementEllipse handle(pDC);
	handle.SetStartCorner(CPoint(253, 491));
	handle.SetHeight(25);
	handle.SetWidth(25);
	handle.Draw(&penWardrobe, &brushHandle);

	handle.SetStartCorner(CPoint(228, 817));
	handle.SetHeight(25);
	handle.SetWidth(25);
	handle.Draw(&penWardrobe, &brushHandle);

	handle.SetStartCorner(CPoint(228, 947));
	handle.SetHeight(25);
	handle.SetWidth(25);
	handle.Draw(&penWardrobe, &brushHandle);

	pDC->RestoreDC(SaveDCIndex);
}
void RGRoom::DrawDoor()
{
	int SaveDCIndex = pDC->SaveDC();
	
	//door
	CPoint doorPoly[4] = {
		CPoint(1747, 213),
		CPoint(1903, 161),
		CPoint(1903, 1070),
		CPoint(1747, 985),
	};
	
	if (m_isDoorOpen){
		CBrush doorbrush(MYBRUSH_COLOR_WOOD);
		CPen doorpen(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);

		_DrawPolygon(MYPEN_COLOR_DEFAULT, MYCOLOR_BLACK, doorPoly);

		pDC->SelectObject(&doorbrush);
		pDC->SelectObject(&doorpen);
		pDC->Rectangle(1490, 213, 1747+1, 985);
	}
	else{
		CPen penHandle(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
		CBrush brushHandle(MYBRUSH_COLOR_HANDLE);

		_DrawPolygon(MYPEN_COLOR_DEFAULT, MYBRUSH_COLOR_WOOD, doorPoly);
		//handle
		pDC->SelectObject(&penHandle);
		pDC->SelectObject(&brushHandle);
		pDC->Ellipse(1862, 630, 1884, 644);
		pDC->Ellipse(1830, 622, 1870, 650);
		
		//keyhole
		CBrush brushKeyHole(MYCOLOR_BLACK);
		pDC->SelectObject(&brushKeyHole);

		pDC->Ellipse(1871, 655, 1885, 673);	
	}

	pDC->RestoreDC(SaveDCIndex);
}
void RGRoom::DrawFloor()
{
	CPoint floorPoly[4] = {
		CPoint(0, 1080),
		CPoint(192, 975 - 2),
		CPoint(1727, 975 - 2),
		CPoint(1920, 1080),
	};
	
	_DrawPolygon(MYPEN_COLOR_DEFAULT, MYBRUSH_COLOR_FLOOR, floorPoly);
}
void RGRoom::DrawRoof()
{
	CPoint roofPoly[4] = {
		CPoint(0, 0),
		CPoint(1920, 0),
		CPoint(1727, 106),
		CPoint(192, 106),
	};

	_DrawPolygon(MYPEN_COLOR_DEFAULT, MYCOLOR_WHITE, roofPoly);
}
void RGRoom::DrawWindow()
{
	int SaveDCIndex = pDC->SaveDC();

	//window frame
	CPen penWindowFrame(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brushWindowFrame(MYBRUSH_COLOR_WOOD);
	pDC->SelectObject(&penWindowFrame);
	pDC->SelectObject(&brushWindowFrame);

	pDC->Rectangle(720, 213, 1204, 707);
	
	//windows
	CPen penWindow(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brushWindow(MYBRUSH_COLOR_WINDOWSGLASS);
	pDC->SelectObject(&penWindow);
	pDC->SelectObject(&brushWindow);

	pDC->Rectangle(741, 243, 953, 683);
	pDC->Rectangle(971, 243, 1183, 683);
	
	//curtain rod
	CPen penCurtainRod(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	pDC->SelectObject(&penCurtainRod);
	pDC->SelectObject(&brushWindowFrame);

	pDC->RoundRect(677, 163, 1247, 213+1, 50, 50);

	//curtain
	CPen penCurtain(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brushCurtain(RGB(250, 254, 160));
	pDC->SelectObject(&penCurtain);
	pDC->SelectObject(&brushCurtain);

	CPoint polyCurtain[4] = {
		CPoint(702,650),
		CPoint(702,213),
		CPoint(1222,213),
		CPoint(1222,650)
	};

	CPoint polyCurtainB[15] = {
		CPoint((int)(702 + (1222 - 702) / 6 * 5), 650 + 20),
		CPoint((int)(702 + (1222 - 702) / 6 * 4), 650 + 20),
		CPoint((int)(702 + (1222 - 702) / 6 * 3), 650 ),
		CPoint((int)(702 + (1222 - 702) / 6 * 2), 650 - 20),
		CPoint((int)(702 + (1222 - 702) / 6 * 1), 650 - 20),
		CPoint(702,650)
	};

	pDC->BeginPath();
		pDC->MoveTo(polyCurtain[0]);
		pDC->PolylineTo(polyCurtain, 4);
		pDC->PolyBezierTo(polyCurtainB,6);
		pDC->EndPath();
	pDC->StrokeAndFillPath();

	pDC->RestoreDC(SaveDCIndex);
}
void RGRoom::DrawGUI()
{
	//proportional scalling
	CSize windowsExtend  = pDC->GetWindowExt();
	CSize viewportExtend = pDC->GetViewportExt();
	pDC->SetMapMode(MM_TEXT);
	//pDC->SetWindowExt(windowsExtend);
	//pDC->SetViewportExt(viewportExtend);

	//drawing gui elements
	CPen pen(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brush(RGB(200,200,200));
	
	int saveDCIndex = pDC->SaveDC();
	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	
	//pDC->RoundRect(21, 9, 176, 96, 7, 7);
	//pDC->RoundRect(15, 15, 130, 110, 12,12);
	pDC->RoundRect(7, 7, 80, 80, 12, 12);

	pDC->RestoreDC(saveDCIndex);

	//restoring proportional scalling
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
}
void RGRoom::DrawKey()
{
	int DCSaveIndex = pDC->SaveDC();
	
	CString keyFileName = CString(MYFILE_KEY);
	//HENHMETAFILE metaFile = GetEnhMetaFile(keyFileName.GetBuffer(keyFileName.GetLength()));
	HENHMETAFILE metaFile = GetEnhMetaFile(keyFileName);
	//proportional scalling
	CSize windowsExtend = pDC->GetWindowExt();
	CSize viewportExtend = pDC->GetViewportExt();
	pDC->SetMapMode(MM_TEXT);
//	pDC->SetWindowExt(windowsExtend);
//	pDC->SetViewportExt(viewportExtend);

	if (m_hasKey) {
		CBrush brush(MYCOLOR_BLACK);
		pDC->SelectObject(&brush);
		//drawing key
		//pDC->Rectangle(17, 17, 70, 70);
		PlayEnhMetaFile(pDC->m_hDC, metaFile,
			CRect(
				17,
				17,
				70,
				70
			));
			
		DeleteEnhMetaFile(metaFile);
	}
	else if (m_isWardrobeOpen) {
		CBrush brush(MYCOLOR_WHITE);
		pDC->SelectObject(&brush);
		/*pDC->Rectangle(
			(int) viewportExtend.cx*0.07, 
			viewportExtend.cy*0.6, 
			(int)viewportExtend.cx*0.07 + (40 + (viewportExtend.cx*0.004)),
			viewportExtend.cy*0.6 + (40 + (viewportExtend.cx*0.004))
		);*/
		PlayEnhMetaFile(pDC->m_hDC, metaFile,
			CRect(
			(int)viewportExtend.cx*0.07,
				viewportExtend.cy*0.6,
				(int)viewportExtend.cx*0.07 + (40 + (viewportExtend.cx*0.004)),
				viewportExtend.cy*0.6 + (40 + (viewportExtend.cx*0.004))
			));

	}

	//restoring proportional scalling
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
	pDC->RestoreDC(DCSaveIndex);
}
void RGRoom::DrawEye()
{
	int saveDCIndex = pDC->SaveDC();
	CPen pen(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brush(RGB(255,255,255));

	pDC->SelectObject(&pen);
	//pDC->SelectObject(&brush);

	CPoint topLeft(1345, 411);
	CPoint bottomRight(1578, 529);
	int offsetCropX = 10;
	
	pDC->BeginPath();
	pDC->MoveTo(1353, 391);



	pDC->Arc(1345, 411, 1578, 529, 1578-10, 450, 1345+10, 450 );
	pDC->Arc(1345, 411-40, 1578, 529-40, 1345 + 10, 450, 1578 - 10, 450 );
	pDC->Ellipse(1420,411, 1500,489);
	
	pDC->EndPath();
	pDC->StrokePath();
	

	pDC->RestoreDC(saveDCIndex);
}
void RGRoom::_DrawPolygon(COLORREF penColor, COLORREF brushColor, CPoint * points)
{
	CPen pen(PS_SOLID, MYPEN_STROKE_DEFAULT, penColor);
	CBrush brush(brushColor);

	int saveDCIndex = pDC->SaveDC();
	pDC->SelectObject(&pen);
	pDC->SelectObject(&brush);
	
	pDC->Polygon(points, 4);

	pDC->RestoreDC(saveDCIndex);
}
void RGRoom::_DrawHitBoxes()
{
	int SaveDCIndex = pDC->SaveDC();

	//window frame
	CPen penWindowFrame(PS_SOLID, 4, RGB(255,0,0));
	pDC->SetBkMode(TRANSPARENT);
	pDC->SelectObject(&penWindowFrame);

	pDC->Rectangle(this->DoorHitBox);
	pDC->Rectangle(this->KeyHitBox);
	pDC->Rectangle(this->WardrboeHitBox);
	
	pDC->RestoreDC(SaveDCIndex);
}

//CLOCK
void RGRoom::DrawClock() 
{
	//scaling
	CSize windowsExtend = pDC->GetWindowExt();
	CSize viewportExtend = pDC->GetViewportExt();
	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);

	//brushes & pens init
	CPen pen(PS_SOLID, MYPEN_STROKE_DEFAULT, MYPEN_COLOR_DEFAULT);
	CBrush brushWood(MYBRUSH_COLOR_WOOD);
	CBrush brushBlack(RGB(0, 0, 0));
	CBrush brushWhite(RGB(255, 255, 255));
	CBrush brushClockYellow(RGB(240, 192, 15));
	CBrush brushHourLabel(RGB(255, 217, 110));
	CPen penClockLine(PS_SOLID, 0, RGB(0, 0, 0));
	CPen penClockLineBold(PS_SOLID, 3, RGB(0, 0, 0));
	CPen penTransparent(PS_NULL, 0, RGB(0, 0, 0));
	CPen penCoordPositive(PS_DOT, 0, RGB(255, 128, 128));
	CPen penCoordNegative(PS_DOT, 0, RGB(0, 128, 128));
	CPen* oldPen;
	XFORM oldXForm;
	
	//Points and sizes init
	CPoint clockCenter(1480+200, 315);
	CPoint oldOrigin;

	int ringDiff = 20;
	int ringRadius = 300;
	int bodyDiff = 15;
	int bodyWidth = 210;
	int bodyHeight = 420;

	int clockRingDiff = 15;
	int clockOuterRing = 80;
	int clockInnerRing = clockOuterRing - clockRingDiff;

	//Drawing clock body
	RGElementRoundRect clockBody(pDC);
	clockBody.SetStartCorner(clockCenter + CPoint(-(bodyWidth/2), 0));
	clockBody.SetWidth(bodyWidth);
	clockBody.SetHeight(bodyHeight);
	clockBody.SetRoundness(40);
	clockBody.Draw(&pen, &brushWood);

	clockBody.SetStartCorner(clockBody.GetStartCorner() + CPoint(bodyDiff, bodyDiff));
	clockBody.SetEndCorner(clockBody.GetEndCorner() - CPoint(bodyDiff, bodyDiff));
	clockBody.Draw(&pen, &brushBlack);

	//Draw Swing
	int swing_height = bodyHeight*0.75;
	int swing_width = 10;
	int swing_ball_radius = 30;

	oldOrigin = pDC->SetWindowOrg(CPoint(-clockCenter.x, -clockCenter.y));
	PrimeniTransformaciju(pDC, -swing_width / 2, 0, 0, 0, swing_angle, &oldXForm);
	RGElementRect swingRect(pDC);
	swingRect.SetStartCorner(CPoint(0, 0));
	swingRect.SetHeight(swing_height);
	swingRect.SetWidth(swing_width);
	swingRect.Draw(&pen, &brushClockYellow);

	RGElementEllipse swingBall(pDC);
	swingBall.SetStartCorner(CPoint(0, swing_height) - CPoint(swing_ball_radius / 1 - swing_width, swing_ball_radius / 1 - swing_width));
	swingBall.SetEndCorner(CPoint(0, swing_height) + CPoint(swing_ball_radius / 1, swing_ball_radius / 1));
	swingBall.Draw(&pen, &brushClockYellow);

	pDC->SetWorldTransform(&oldXForm);
	pDC->SetWindowOrg(oldOrigin);

	//Drawing clock border ring
	RGElementEllipse clockRing(pDC);
	clockRing.SetStartCorner(clockCenter - CPoint(ringRadius / 2, ringRadius / 2));
	clockRing.SetWidth(ringRadius);
	clockRing.SetHeight(ringRadius);
	clockRing.Draw(&pen, &brushWood);

	clockRing.SetStartCorner(clockRing.GetStartCorner() + CPoint(ringDiff, ringDiff));
	clockRing.SetEndCorner(clockRing.GetEndCorner() - CPoint(ringDiff, ringDiff));
	clockRing.Draw(&pen, &brushClockYellow);
	
	//Drawing clock line border
	RGElementEllipse clockLineRing(pDC);
	CPoint clockLineRingOuterStart(clockCenter - CPoint(clockOuterRing, clockOuterRing));
	CPoint clockLineRingOuterEnd(clockCenter + CPoint(clockOuterRing, clockOuterRing));
	
	clockLineRing.SetStartCorner(clockLineRingOuterStart);
	clockLineRing.SetEndCorner(clockLineRingOuterEnd);
	clockLineRing.Draw(&penClockLine, &brushWhite);
	
	CPoint clockLineRingInnerStart(clockLineRing.GetStartCorner() + CPoint(clockRingDiff, clockRingDiff));
	CPoint clockLineRingInnerEnd(clockLineRing.GetEndCorner() - CPoint(clockRingDiff, clockRingDiff));

	clockLineRing.SetStartCorner(clockLineRingInnerStart);
	clockLineRing.SetEndCorner(clockLineRingInnerEnd);
	clockLineRing.Draw(&penClockLine, &brushWhite);

	//Draw clock lines
	oldOrigin = pDC->SetWindowOrg(CPoint(-clockCenter.x,-clockCenter.y));
	oldPen = pDC->SelectObject(&penCoordPositive);
	
	pDC->MoveTo(0, 0);
	pDC->LineTo(clockOuterRing*3, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, clockInnerRing*3);
	pDC->MoveTo(0, 0);
	pDC->SelectObject(&penCoordNegative);
	pDC->MoveTo(0, 0);
	pDC->LineTo(-clockOuterRing*3, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, -clockInnerRing*3);
	pDC->SelectObject(oldPen);

	for (int i = 0; i < 60; i++)
	{
		PrimeniTransformaciju(pDC, 0, clockOuterRing, 0, 0, i*(MATH_PI * 6) / 180, &oldXForm);
		int penwidth = (i % 5 == 0) ? 3 : 1;
		CPen pen(PS_SOLID, penwidth,MYCOLOR_BLACK);
		CPen* oldPen = pDC->SelectObject(&pen);
		
		pDC->MoveTo(0, 0);
		pDC->LineTo(0, clockRingDiff);
		pDC->SelectObject(oldPen);
		pDC->SelectObject(oldPen);
		pDC->SetWorldTransform(&oldXForm);
	}
	pDC->SetWindowOrg(oldOrigin);
	

	//Drawing hours labels
	CString labels[12] = { 
		CString("XII"), 
		CString("I"), 
		CString("II"),
		CString("III"),
		CString("IV"),
		CString("V"),
		CString("VI"), 
		CString("VII"), 
		CString("VIII"), 
		CString("IX"), 
		CString("X"),
		CString("XI") 
	};
	
	CFont clockFont;
	clockFont.CreateFontW(25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	pDC->SetTextAlign(TA_CENTER);
	pDC->SetBkMode(TRANSPARENT);
	CFont* oldFont = (CFont*)pDC->SelectObject(&clockFont);
	oldOrigin = pDC->SetWindowOrg(CPoint(-clockCenter.x, -clockCenter.y));
	for (int i = 0; i < 12; i++)
	{
		
		PrimeniTransformaciju(pDC, 0, clockOuterRing + 25, 0, 0, i* (MATH_PI * 30 / 180), &oldXForm);
		RGElementEllipse labelCircle(pDC);
		labelCircle.SetStartCorner(CPoint(-20, -20));
		labelCircle.SetEndCorner(CPoint(20, 20));
		labelCircle.Draw(&penTransparent,&brushHourLabel);
		pDC->SetWorldTransform(&oldXForm);

		PrimeniTransformaciju(pDC, 0, clockOuterRing + 40, 0, 0, i* (MATH_PI * 30 / 180), &oldXForm);
		pDC->TextOutW(0, 0, labels[i]);
		pDC->SetWorldTransform(&oldXForm);
	}
	
	pDC->SetWindowOrg(oldOrigin);
	
	//Drawing small cursor
	CString smallCursorFilename(MYFILE_SMALL_CURSOR);
	HENHMETAFILE mf_small = GetEnhMetaFile(smallCursorFilename);
	oldOrigin = pDC->SetWindowOrg(CPoint(-clockCenter.x, -clockCenter.y));
	CRect cursorSmall;
	int cusor_small_width = clockLineRingInnerEnd.x - clockLineRingInnerStart.x;
	cursorSmall.SetRect(0, 0, cusor_small_width*0.35, cusor_small_width*0.75);
	PrimeniTransformaciju(pDC, -cursorSmall.Width() / 2, cursorSmall.Height()*0.68, 0, 0,small_cursor_angle,&oldXForm);
	PlayEnhMetaFile(pDC->m_hDC, mf_small, cursorSmall);
	pDC->SetWorldTransform(&oldXForm);
	pDC->SetWindowOrg(oldOrigin);

	////Drawing big cursor
	CString bigCursorFilename(MYFILE_BIG_CURSOR);
	HENHMETAFILE mf_big = GetEnhMetaFile(bigCursorFilename);
	oldOrigin = pDC->SetWindowOrg(CPoint(-clockCenter.x, -clockCenter.y));
	CRect cursorBig;
	int cusor_big_width = clockLineRingOuterEnd.x - clockLineRingOuterStart.x;
	cursorBig.SetRect(0,-3,cusor_big_width*0.15,cusor_big_width*0.75);
	PrimeniTransformaciju(pDC, -cursorBig.Width() / 2, cursorBig.Height()*0.68, 0, 0, big_cursor_angle, &oldXForm);
	PlayEnhMetaFile(pDC->m_hDC, mf_big, cursorBig);
	pDC->SetWorldTransform(&oldXForm);
	pDC->SetWindowOrg(oldOrigin);



	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
}
void RGRoom::RotateSmallCursor()
{
	small_cursor_angle += 30 * MATH_PI / 180;
	RotateSwing();
}
void RGRoom::RotateBigCursor()
{
	big_cursor_angle += 6 * MATH_PI / 180;
	small_cursor_angle += (30 * MATH_PI / 180) / 60;
	RotateSwing();
}
void RGRoom::RotateSwing()
{
	if (swing_angle >= 9 * MATH_PI / 180)
		swing_direction = -1;
	else if (swing_angle <= -9 * MATH_PI / 180)
		swing_direction = 1;

	swing_angle += swing_direction * MATH_PI / 180;
}
void RGRoom::DrawFlower(CPoint center, int innerRadius, int outerRadius)
{
	//scaling
	CSize windowsExtend = pDC->GetWindowExt();
	CSize viewportExtend = pDC->GetViewportExt();
	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);

	XFORM oldXFORM;
	CPoint oldOrigin;
	CPen* oldPen;
	CPen penFlower(PS_SOLID, 0, RGB(255, 0, 255));
	CBrush brushFlower(MYCOLOR_WHITE);
	CBrush brushFlowerPart(RGB(128,15,187));
	CBrush brushFlowerCenter(MYBRUSH_COLOR_WOOD);

	int flowerPartRadius = outerRadius - innerRadius;

	RGElementEllipse outerRing(pDC);
	RGElementEllipse innerRing(pDC);

	//coord drawing
	outerRing.SetStartCorner(center - CPoint(outerRadius, outerRadius));
	outerRing.SetEndCorner(center + CPoint(outerRadius, outerRadius));
	outerRing.Draw(&penFlower, &brushFlower);

	innerRing.SetStartCorner(center - CPoint(innerRadius, innerRadius));
	innerRing.SetEndCorner(center + CPoint(innerRadius, innerRadius));
	innerRing.Draw(&penFlower, &brushFlowerCenter);
	//end coord drawing

	oldOrigin = pDC->SetWindowOrg(CPoint(-center.x, -center.y));

	int count = (2 * (innerRadius + flowerPartRadius / 2)*MATH_PI) / flowerPartRadius*1.5;
	for (int i = 0; i < count; i++)
	{

		PrimeniTransformaciju(pDC, innerRadius, innerRadius, 0, 0, i* (MATH_PI * 30 / 180), &oldXFORM);
		RGElementEllipse flowerPart(pDC);
		flowerPart.SetStartCorner(CPoint(-flowerPartRadius/2, -flowerPartRadius/2));
		flowerPart.SetHeight(flowerPartRadius);
		flowerPart.SetWidth(flowerPartRadius*1.5);
		flowerPart.Draw(&penFlower, &brushFlowerPart);
		pDC->SetWorldTransform(&oldXFORM);

	}
	pDC->SetWindowOrg(oldOrigin);

	


	//DEBUG
	CPen penCoordPositive(PS_DOT, 0, RGB(255, 128, 128));
	CPen penCoordNegative(PS_DOT, 0, RGB(0, 128, 128));
	oldOrigin = pDC->SetWindowOrg(CPoint(-center.x, -center.y));
	oldPen = pDC->SelectObject(&penCoordPositive);
	pDC->MoveTo(0, 0);
	pDC->LineTo(outerRadius*2, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, outerRadius * 2);
	pDC->MoveTo(0, 0);
	pDC->SelectObject(&penCoordNegative);
	pDC->MoveTo(0, 0);
	pDC->LineTo(-outerRadius * 2, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, -outerRadius * 2);
	pDC->SelectObject(oldPen);
	pDC->SetWindowOrg(oldOrigin);


	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
}

//PAINTING
void RGRoom::PaintingEnableColor(char color)
{
	switch (color)
	{
	case 'R':
		paintingColorRed = !paintingColorRed;
		break;
	case 'G':
		paintingColorGreen = !paintingColorGreen;
		break;
	case 'B':
		paintingColorBlue = !paintingColorBlue;
		break;
	}
}
void RGRoom::DrawPainting()
{
	//scaling
	CSize windowsExtend = pDC->GetWindowExt();
	CSize viewportExtend = pDC->GetViewportExt();
	pDC->SetMapMode(MM_ISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
	CPoint oldOrigin;
	CPoint newOrigin;
	newOrigin.x = canvasSize.x*0.77;
	newOrigin.y = canvasSize.y*0.20;
	oldOrigin = pDC->SetWindowOrg(CPoint(-newOrigin.x, -newOrigin.y));

	
	DImage frame;
	frame.Load(CString(MYFILE_PAINTING_FRAME));
	float scaleFactor = 0.75;
	CPoint part1_offset((-75 + 85) * scaleFactor, (-57 + 85) * scaleFactor);
	CPoint part2_offset((78) * scaleFactor, (170) * scaleFactor);
	CPoint part3_offset((400) * scaleFactor, (355) * scaleFactor);
	CPoint part4_offset((256) * scaleFactor, (211) * scaleFactor);


	DrawBitmapDIMAGE(pDC,0,0, scaleFactor,(DImage*) this->frame);
	DrawPuzzlePartDIMAGE(pDC, part1_offset.x + 0, part1_offset.y - 3, scaleFactor, 0, false, paintingColorRed, paintingColorGreen, paintingColorBlue, (DImage*)this->puzzle1);
	DrawPuzzlePartDIMAGE(pDC, part2_offset.x + 0, part2_offset.y + 0, scaleFactor, Angle(-34), false, paintingColorRed, paintingColorGreen, paintingColorBlue, (DImage*)this->puzzle2);
	DrawPuzzlePartDIMAGE(pDC, part3_offset.x + 0, part3_offset.y + 0, scaleFactor, Angle(26 + 90), false, paintingColorRed, paintingColorGreen, paintingColorBlue, (DImage*)this->puzzle3);
	DrawPuzzlePartDIMAGE(pDC, part4_offset.x + 0, part4_offset.y + 3, scaleFactor, Angle(-59), true, paintingColorRed, paintingColorGreen, paintingColorBlue, (DImage*)this->puzzle4);
	
	/*
	DrawBitmap(pDC, 0, 0, scaleFactor, CString(MYFILE_PAINTING_FRAME));
	DrawPuzzlePart(pDC, part1_offset.x + 0, part1_offset.y - 3, scaleFactor, 0, false, paintingColorRed, paintingColorGreen, paintingColorBlue, CString(MYFILE_PUZZLE_PART1));
	DrawPuzzlePart(pDC, part2_offset.x + 0, part2_offset.y + 0, scaleFactor, Angle(-34), false, paintingColorRed, paintingColorGreen, paintingColorBlue, CString(MYFILE_PUZZLE_PART2));
	DrawPuzzlePart(pDC, part3_offset.x + 0, part3_offset.y + 0, scaleFactor, Angle(26+90), false, paintingColorRed, paintingColorGreen, paintingColorBlue, CString(MYFILE_PUZZLE_PART3));
	DrawPuzzlePart(pDC, part4_offset.x + 0, part4_offset.y + 3, scaleFactor, Angle(-59), true, paintingColorRed, paintingColorGreen, paintingColorBlue, CString(MYFILE_PUZZLE_PART4));
	*/


	pDC->SetWindowOrg(oldOrigin);
	//DEBUG
	CPen penCoordPositive(PS_DOT, 0, RGB(128, 255, 128));
	CPen penCoordNegative(PS_DOT, 0, RGB(255, 128, 128));
	oldOrigin = pDC->SetWindowOrg(CPoint(-newOrigin.x, -newOrigin.y));
	CPen* oldPen = pDC->SelectObject(&penCoordPositive);
	int lineLenght = 500;
	pDC->MoveTo(0, 0);
	pDC->LineTo(lineLenght, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, lineLenght);
	pDC->MoveTo(0, 0);
	pDC->SelectObject(&penCoordNegative);
	pDC->MoveTo(0, 0);
	pDC->LineTo(-lineLenght, 0);
	pDC->MoveTo(0, 0);
	pDC->LineTo(0, -lineLenght);
	pDC->SelectObject(oldPen);
	pDC->SetWindowOrg(oldOrigin);

	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(windowsExtend);
	pDC->SetViewportExt(viewportExtend);
}

