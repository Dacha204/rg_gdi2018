#include "stdafx.h"
#include "RGElementEllipse.h"


RGElementEllipse::RGElementEllipse(CDC * pDC)
	:RGDrawElement(pDC)
{
}

RGElementEllipse::RGElementEllipse(CDC * pDC, CPoint startCorner, CPoint endCorner)
	: RGDrawElement(pDC, startCorner, endCorner)
{
}

RGElementEllipse::RGElementEllipse(CDC * pDC, CPoint startCorner, int width, int height)
	: RGDrawElement(pDC, startCorner, width, height)
{
}

RGElementEllipse::~RGElementEllipse()
{
}

void RGElementEllipse::DrawElement()
{
	pDC->Ellipse(
		getStartCorner()->x, getStartCorner()->y,
		getEndCorner()->x, getEndCorner()->y
	);
}
