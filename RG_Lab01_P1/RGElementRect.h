#pragma once
#include "RGDrawElement.h"
class RGElementRect :
	public RGDrawElement
{
public:
	RGElementRect(CDC* pDC);
	RGElementRect(CDC* pDC, CPoint startCorner, CPoint endCorner);
	RGElementRect(CDC* pDC, CPoint startCorner, int width, int height);
	~RGElementRect();

	void DrawElement();
};

