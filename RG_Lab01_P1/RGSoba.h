#pragma 
class RGRoom
{
private:
	CDC* pDC;
	CPoint canvasSize;
	CView* parrentView;
	//keyfile
	
	CRect WardrboeHitBox;
	CRect DoorHitBox;
	CRect KeyHitBox;
	
	bool paintingColorRed;
	bool paintingColorBlue;
	bool paintingColorGreen;

public:
	
	bool m_isDoorOpen;
	bool m_isWardrobeOpen;
	bool m_hasKey;
	double small_cursor_angle;
	double big_cursor_angle;
	double swing_angle;
	double swing_direction;
	void* frame; //hack
	void* puzzle1; //hack
	void* puzzle2; //hack
	void* puzzle3; //hack
	void* puzzle4; //hack

	RGRoom();
	~RGRoom();
	
	CPoint getCanvasSize() { return this->canvasSize; }
	void DrawRoom(CDC* pDC);
	bool SetOpenDoor();
	bool SetWardrobeOpen();
	bool SetHasKey();
	CRect GetWardrobeHitBox();
	CRect GetDoorHitBox();
	CRect GetKeyHitBox();
	void RotateSmallCursor();
	void RotateBigCursor();
	void RotateSwing();

	void DrawFlower(CPoint center, int innerRadius, int outerRadius);

	void PaintingEnableColor(char color);

	void DrawPainting();

private:
	void DrawWalls();
	void DrawWardrobe();
	void DrawDoor();
	void DrawFloor();
	void DrawRoof();
	void DrawWindow();
	void DrawGUI();	
	void DrawKey();
	void DrawEye();

	//utilities
	void _DrawPolygon(COLORREF penColor, COLORREF brushColor, CPoint * points);
	void _DrawHitBoxes();
	void DrawClock();

};

