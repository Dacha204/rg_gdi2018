#pragma once
#include <iostream>
#include <vector>

class RGDrawElement
{
private:
	int DCSaveIndex = -1;

protected:
	std::vector<CPoint> points;
	CDC* pDC;

public:
	RGDrawElement(CDC* pDC);
	RGDrawElement(CDC* pDC, CPoint startCorner, CPoint endCorner);
	RGDrawElement(CDC* pDC, CPoint startCorner, int width, int height);
	virtual ~RGDrawElement();

	virtual int GetWidth();
	virtual int GetHeight();
	virtual void SetWidth(int width);
	virtual void SetHeight(int height);
	virtual void SetStartCorner(CPoint point);
	virtual void SetEndCorner(CPoint point);
	virtual void SetOffsetLeft(int x);
	virtual void SetOffsetTop(int y);
	
	virtual CPoint GetStartCorner();
	virtual CPoint GetEndCorner();

	void SetOffset(CPoint offsetPoint);

	void Draw(CPen* pen, CBrush* brush);

protected:
	virtual CPoint* getStartCorner();
	virtual CPoint* getEndCorner();
	virtual void DrawElement() = 0;

private:
	void DrawInit(CPen * pen, CBrush * brush);
	void DrawFinish();
};

