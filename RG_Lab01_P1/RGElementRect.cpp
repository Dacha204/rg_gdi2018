#include "stdafx.h"
#include "RGElementRect.h"


RGElementRect::RGElementRect(CDC * pDC)
	:RGDrawElement(pDC)
{
}

RGElementRect::RGElementRect(CDC * pDC, CPoint startCorner, CPoint endCorner)
	: RGDrawElement(pDC, startCorner, endCorner)
{
}

RGElementRect::RGElementRect(CDC * pDC, CPoint startCorner, int width, int height)
	: RGDrawElement(pDC, startCorner, width, height)
{
}

RGElementRect::~RGElementRect()
{
}

void RGElementRect::DrawElement()
{
	pDC->Rectangle(
		getStartCorner()->x, getStartCorner()->y,
		getEndCorner()->x, getEndCorner()->y
	);
}
