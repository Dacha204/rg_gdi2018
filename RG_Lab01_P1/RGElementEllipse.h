#pragma once
#include "RGDrawElement.h"
class RGElementEllipse :
	public RGDrawElement
{
public:
	RGElementEllipse(CDC* pDC);
	RGElementEllipse(CDC* pDC, CPoint startCorner, CPoint endCorner);
	RGElementEllipse(CDC* pDC, CPoint startCorner, int width, int height);
	~RGElementEllipse();

	void DrawElement();
};

