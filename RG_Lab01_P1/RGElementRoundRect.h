#pragma once
#include "RGDrawElement.h"
class RGElementRoundRect :
	public RGDrawElement

{
private:
	CPoint roundness;

public:
	RGElementRoundRect(CDC* pDC);
	RGElementRoundRect(CDC* pDC, CPoint startCorner, CPoint endCorner);
	RGElementRoundRect(CDC* pDC, CPoint startCorner, int width, int height);
	~RGElementRoundRect();

	void DrawElement();
	void SetRoundness(int pixelRoundness);
	void SetRoundness(int RoundnessX, int RoundnessY);
	CPoint GetRoundness();
};

