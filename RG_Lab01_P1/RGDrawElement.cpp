#include "stdafx.h"
#include "RGDrawElement.h"


RGDrawElement::RGDrawElement(CDC * pDC)
{
	this->pDC = pDC;
	CPoint StartCorner(0, 0);
	CPoint EndCorner(0, 0);
	points.push_back(StartCorner);
	points.push_back(EndCorner);

}

RGDrawElement::RGDrawElement(CDC * pDC, CPoint startCorner, CPoint endCorner)
{
	this->pDC = pDC;
	points.push_back(startCorner);
	points.push_back(endCorner);
}

RGDrawElement::RGDrawElement(CDC * pDC, CPoint startCorner, int width, int height)
{
	this->pDC = pDC;
	points.push_back(startCorner);
	CPoint EndCorner;
	EndCorner = startCorner + CPoint(width, height);
	points.push_back(EndCorner);
}

RGDrawElement::~RGDrawElement()
{
}

int RGDrawElement::GetWidth()
{
	return getEndCorner()->x - getStartCorner()->x;
}

int RGDrawElement::GetHeight()
{
	return getEndCorner()->y - getStartCorner()->y;
}

void RGDrawElement::SetWidth(int width)
{
	getEndCorner()->x = getStartCorner()->x + width;
}

void RGDrawElement::SetHeight(int height)
{
	getEndCorner()->y = getStartCorner()->y + height;
}

void RGDrawElement::SetStartCorner(CPoint point)
{
	points[0] = point;
}

void RGDrawElement::SetEndCorner(CPoint point)
{
	points[1] = point;
}

void RGDrawElement::SetOffsetLeft(int x)
{
	points[0].x = x;
}

void RGDrawElement::SetOffsetTop(int y)
{
	points[0].y = y;
}

CPoint RGDrawElement::GetStartCorner()
{
	return points[0];
}

CPoint RGDrawElement::GetEndCorner()
{
	return points[1];
}

void RGDrawElement::SetOffset(CPoint offsetPoint)
{
	SetStartCorner(offsetPoint);
}
void RGDrawElement::Draw(CPen * pen, CBrush * brush)
{
	DrawInit(pen,brush);
	DrawElement();
	DrawFinish();
}

CPoint* RGDrawElement::getStartCorner()
{
	return &points[0];
}

CPoint* RGDrawElement::getEndCorner()
{
	return &points[1];
}

void RGDrawElement::DrawInit(CPen * pen, CBrush * brush)
{
	DCSaveIndex = pDC->SaveDC();
	pDC->SelectObject(pen);
	pDC->SelectObject(brush);
}

void RGDrawElement::DrawFinish()
{
	pDC->RestoreDC(DCSaveIndex);
}
