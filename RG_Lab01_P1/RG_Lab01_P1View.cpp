
// RG_Lab01_P1View.cpp : implementation of the CRGLab01P1View class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "RG_Lab01_P1.h"
#endif

#include "RG_Lab01_P1Doc.h"
#include "RG_Lab01_P1View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define MATH_PI 3.14159265359

// CRGLab01P1View

IMPLEMENT_DYNCREATE(CRGLab01P1View, CView)

BEGIN_MESSAGE_MAP(CRGLab01P1View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_ERASEBKGND()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()

// CRGLab01P1View construction/destruction

CRGLab01P1View::CRGLab01P1View()
{
	// TODO: add construction code here
	this->frame_img = new DImage();
	this->puzzle1 = new DImage();
	this->puzzle2 = new DImage();
	this->puzzle3 = new DImage();
	this->puzzle4 = new DImage();

	frame_img->Load(CString(MYFILE_PAINTING_FRAME));
	puzzle1->Load(CString(MYFILE_PUZZLE_PART1));
	puzzle2->Load(CString(MYFILE_PUZZLE_PART2));
	puzzle3->Load(CString(MYFILE_PUZZLE_PART3));
	puzzle4->Load(CString(MYFILE_PUZZLE_PART4));
}

CRGLab01P1View::~CRGLab01P1View()
{
	if (this->frame_img != NULL)
		delete frame_img;
	if (this->puzzle1 != NULL)
		delete puzzle1;
	if (this->puzzle2 != NULL)
		delete puzzle2;
	if (this->puzzle3 != NULL)
		delete puzzle3;
	if (this->puzzle4 != NULL)
		delete puzzle4;
}

BOOL CRGLab01P1View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CRGLab01P1View drawing

void CRGLab01P1View::OnDraw(CDC* pDC)
{
	CRGLab01P1Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	CBitmap memBitmap;

	CRect windowViewRect;
	GetClientRect(&windowViewRect);
	CDC memDC;

	memDC.CreateCompatibleDC(pDC);

	pDC->SetGraphicsMode(GM_ADVANCED);
	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(room.getCanvasSize().x, room.getCanvasSize().y);
	pDC->SetViewportExt(windowViewRect.right, windowViewRect.bottom);
	
	memDC.SetGraphicsMode(GM_ADVANCED);
	memDC.SetMapMode(MM_ANISOTROPIC);
	memDC.SetWindowExt(room.getCanvasSize().x, room.getCanvasSize().y);
	memDC.SetViewportExt(windowViewRect.right, windowViewRect.bottom);
	memBitmap.CreateCompatibleBitmap(pDC, room.getCanvasSize().x, room.getCanvasSize().y);
	memDC.SelectObject(&memBitmap);


	room.frame = frame_img;
	room.puzzle1 = this->puzzle1;
	room.puzzle2 = this->puzzle2;
	room.puzzle3 = this->puzzle3;
	room.puzzle4 = this->puzzle4;
	//room.DrawRoom(pDC);
	room.DrawRoom(&memDC);

	bool stat = pDC->BitBlt(0, 0, room.getCanvasSize().x, room.getCanvasSize().y, &memDC, 0, 0, SRCCOPY);
}


// CRGLab01P1View printing

BOOL CRGLab01P1View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CRGLab01P1View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CRGLab01P1View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CRGLab01P1View diagnostics

#ifdef _DEBUG
void CRGLab01P1View::AssertValid() const
{
	CView::AssertValid();
}

void CRGLab01P1View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CRGLab01P1Doc* CRGLab01P1View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRGLab01P1Doc)));
	return (CRGLab01P1Doc*)m_pDocument;
}
#endif //_DEBUG


// CRGLab01P1View message handlers


void CRGLab01P1View::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	if (CheckHitBox(point, room.GetWardrobeHitBox()))
	{
		room.SetWardrobeOpen();
		Invalidate();
		return;
	}

	if (CheckHitBox(point, room.GetKeyHitBox()))
	{
		room.SetHasKey();
		Invalidate();
		return;
	}

	if (CheckHitBox(point, room.GetDoorHitBox()))
	{
		room.SetOpenDoor();
		Invalidate();
		return;
	}

	CView::OnLButtonDown(nFlags, point);
}

BOOL CRGLab01P1View::CheckHitBox(CPoint point, CRect hitBoxRect)
{
	CRect window;
	CRgn hitBoxRegion;
	GetClientRect(&window);
	bool result;

	hitBoxRegion.CreateRectRgn(
		(window.right*hitBoxRect.left) / (room.getCanvasSize().x),
		(window.bottom*hitBoxRect.top) / (room.getCanvasSize().y),
		(window.right*hitBoxRect.right) / (room.getCanvasSize().x),
		(window.bottom*hitBoxRect.bottom) / (room.getCanvasSize().y)
	);

	result = hitBoxRegion.PtInRegion(point);
	hitBoxRegion.DeleteObject();

	return result;
}

BOOL CRGLab01P1View::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	return true;
	return CView::OnEraseBkgnd(pDC);
}


void CRGLab01P1View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch ((char)nChar)
	{
	case 'M':
		room.RotateSmallCursor();
		break;
	case 'V':
		room.RotateBigCursor();
		break;
	case 'R':
		room.PaintingEnableColor('R');
		break;
	case 'G':
		room.PaintingEnableColor('G');
		break;
	case 'B':
		room.PaintingEnableColor('B');
		break;
	}
	Invalidate();

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}
