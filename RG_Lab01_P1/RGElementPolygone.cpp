#include "stdafx.h"
#include "RGElementPolygone.h"

RGElementPolygone::RGElementPolygone(CDC * pDC)
	:RGDrawElement(pDC)
{
}

//RGElementPolyLine::RGElementPolyLine(CDC * pDC, CPoint start, CPoint end)
//	: RGDrawElement(pDC, start, end)
//{
//	points.clear();
//}
//
//RGElementPolyLine::RGElementPolyLine(CDC * pDC, CPoint start, int width, int height)
//	: RGDrawElement(pDC, startCorner, width, height)
//{
//	points.clear();
//}

RGElementPolygone::~RGElementPolygone()
{
}

void RGElementPolygone::DrawElement()
{
	pDC->Polygon(&points[2], points.size()-2); //skip first two
}

void RGElementPolygone::AddNextPoint(CPoint point)
{
	points.push_back(point);
}
