
// RG_Lab01_P1View.h : interface of the CRGLab01P1View class
//

#pragma once
#include "DImage.h"

#ifndef MYFILE_PAINTING_FRAME
	#define MYFILE_PAINTING_FRAME "Ram.bmp"
	#define MYFILE_PUZZLE_PART1 "part1.bmp"
	#define MYFILE_PUZZLE_PART2 "part2.bmp"
	#define MYFILE_PUZZLE_PART3 "part3.bmp"
	#define MYFILE_PUZZLE_PART4 "part4.bmp"
#endif // !1

class CRGLab01P1View : public CView
{
protected: // create from serialization only
	CRGLab01P1View();
	DECLARE_DYNCREATE(CRGLab01P1View)

	RGRoom room;
	DImage* frame_img;
	DImage* puzzle1;
	DImage* puzzle2;
	DImage* puzzle3;
	DImage* puzzle4;

// Attributes
public:
	CRGLab01P1Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CRGLab01P1View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	BOOL CheckHitBox(CPoint point, CRect hitBoxRect);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
};

#ifndef _DEBUG  // debug version in RG_Lab01_P1View.cpp
inline CRGLab01P1Doc* CRGLab01P1View::GetDocument() const
   { return reinterpret_cast<CRGLab01P1Doc*>(m_pDocument); }
#endif

