
// RG_Lab01_P1.h : main header file for the RG_Lab01_P1 application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols
#include "RGSoba.h"

// CRGLab01P1App:
// See RG_Lab01_P1.cpp for the implementation of this class
//

class CRGLab01P1App : public CWinApp
{
public:
	CRGLab01P1App();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CRGLab01P1App theApp;
